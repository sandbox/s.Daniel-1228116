This module utilizes the ajaxit plugin to convert a Drupal site into an ajax site. To use this module:

1. Download the ajaxit package from http://plugins.jquery.com/project/ajaxit
2. Place the files in /sites/all/libraries/ajaxit. The files should be located in:
   /sites/all/libraries/ajaxit/ajaxit.js
   /sites/all/libraries/ajaxit/jquery.history.js
3. Enable the module
4. Go to settings and specify the jquery wrapper and other javascript callback functions (check documentation below)

For the plugin documentation, visit http://www.o-minds.com/products/ajaxit
